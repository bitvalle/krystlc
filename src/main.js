import Vue from 'vue'
import App from './App'
import Buefy from 'buefy'
import timeago from '../node_modules/timeago.js'

Vue.use(Buefy)

Vue.filter('formatDate', (value) => timeago().format(value))

new Vue({
  el: '#app',
  render: h => h(App)
})
